<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'v1'], function () {

    /** TESTING */
    Route::get('/test', function() {
        return 'Hello test';
    });

    /** Cache */
    Route::get('/clear-cache', function() {
        Artisan::call('config:cache');
        Artisan::call('cache:clear');
        Artisan::call('route:clear');

        return "Cache is cleared";
    });

    //EVENTS
    Route::group(['prefix' => 'event', 'namespace' => 'App\Http\Controllers\v1\Api\Events'], function () {
        Route::get('events', 							'EventController@index');
        Route::get('events/actors/{id}', 					'EventController@show');
        Route::get('actors/streak', 						'EventController@actorMaximumStreak');
        Route::get('actors', 							'EventController@allActors');
        Route::post('events', 						'EventController@store');
        Route::post('actors', 						'EventController@update');
        Route::delete('erase', 					'EventController@destroy');
    });

    /**
     * THIS SHOULD ALWAYS BE THE ENDING OF THIS PAGE
     */
    Route::fallback(function () {
        return response()->json([
            'error' => true,
            'message' => 'Route don\'t exist',
            'data' => null
        ], 404);
    });
});
