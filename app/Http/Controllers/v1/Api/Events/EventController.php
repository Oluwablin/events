<?php

namespace App\Http\Controllers\v1\Api\Events;

use App\Models\Event;
use App\Models\User;
use App\Models\Repo;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Resources\EventResource;
use Validator, DB;
use Illuminate\Support\Str;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     *  @desc Return all the events
     *  @route GET /api/v1/event/events

     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $events = Event::with(['actor', 'repo'])->orderBy('id', 'ASC')->get();

        return response()->json([
            "error" => false,
            "message" => null,
            "data" => EventResource::collection($events),
        ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     *  @desc Return all the events by actor maximum streak
     *  @route GET /api/v1/event/actors/streak
     *
     * @return \Illuminate\Http\Response
     */
    public function actorMaximumStreak(Request $request)
    {
        $actors = User::leftjoin('events', 'users.id', '=', 'events.user_id')
        ->select('users.*')
        ->selectRaw('user_id, count(*) as count')
        ->orderBy(DB::raw('user_id IS NULL'))
        ->groupBy('users.id')
        ->groupBy('events.user_id')
        ->orderBy('count', 'DESC')
        ->orderBy('created_at', 'DESC')
        ->get();

        return response()->json([
            "error" => false,
            "message" => null,
            "data" => $actors,
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     *  @desc Create an event
     *  @route POST /api/v1/event/events
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $credentials = $request->all();
        $rules = [
            ['avatar' => 'required'],
            ['avatar' => 'image|mimes:jpg,png'],
        ];

        $validateAvatar = Validator::make($credentials, $rules[0]);
        if ($validateAvatar->fails()) {
            return response()->json([
                'error' => true,
                'message' => 'Avatar is required',
                'data' => null
            ], 422);
        }

        $validateAvatarType = Validator::make($credentials, $rules[1]);
        if($validateAvatarType->fails()) {
            return response()->json([
                'error'=> true,
                'message'=> $validateAvatarType->messages()->all(),
                'data' => null
            ], 422);
        }

        try {
            // Start transaction!
            DB::beginTransaction();

            $avatar = $request->file('avatar');
            $fileExt = $avatar->getClientOriginalExtension();
            $name =  date("Y-m-d").'_'.time().'.'.$fileExt;
            $avatarName = config('app.url') . "/assets/avatars/$name";

            $storeFile = $avatar->move(public_path('assets/avatars'), $name);

            $actor = new User;
            $actor->login = Str::random(10);
            $actor->avatar_url = $avatarName;
            if($actor->save()){
                $repo = new Repo;
                $repo->name = $request->name;
                $repo->url = $request->url;
                if($repo->save()){
                    $event = new Event;
                    $event->type = $request->type;
                    $event->user_id = $actor->id;
                    $event->repo_id = $repo->id;
                    $event->save();
                }
            }
            DB::commit();
            return response()->json([
                'error'=> false,
                'message'=> 'Event created successfully',
                'data' => null
            ], 201);
        } catch (\Throwable $th) {
            DB::rollback();
            $error = true;
            $message = $th->getMessage();
            $data = [];
            response()->json([$error, $message, $data], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     *  @desc Return all the events by actor id
     *  @route GET /api/v1/event/events/actors/{id}
     *
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $actor = User::find($id);
        if (!$actor) {
            return response()->json([
                'error'=> true,
                'message'=> 'This actor cannot be found.',
                'data' => null
            ], 404);
        }
        $events = Event::where('user_id', $actor->id)->with(['actor', 'repo'])->orderBy('id', 'ASC')->get();

        return response()->json([
            "error" => false,
            "message" => null,
            "data" => EventResource::collection($events),
        ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     *  @desc Return all the actors by number of events
     *  @route GET /api/v1/event/actors
     *
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function allActors(Request $request)
    {
        $actors = User::leftjoin('events', 'users.id', '=', 'events.user_id')
        ->select('users.*')
        ->selectRaw('user_id, count(*) as count')
        ->orderBy(DB::raw('user_id IS NULL'))
        ->groupBy('users.id')
        ->groupBy('events.user_id')
        ->orderBy('count', 'DESC')
        ->orderBy('created_at', 'DESC')
        ->orderBy('login', 'DESC')
        ->get();

        return response()->json([
            "error" => false,
            "message" => null,
            "data" => $actors,
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     *  @desc Update an actor
     *  @route POST /api/v1/event/actors
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $actor = User::find($request->id);
        if (!$actor) {
            return response()->json([
                'error'=> true,
                'message'=> 'This actor cannot be found.',
                'data' => null
            ], 404);
        }

        $credentials = $request->only('avatar');
        $rules = [
            ['avatar' => 'required'],
            ['avatar' => 'image|mimes:jpg,png'],
        ];

        $validateAvatar = Validator::make($credentials, $rules[0]);
        if ($validateAvatar->fails()) {
            return response()->json([
                'error' => true,
                'message' => 'Avatar is required',
                'data' => null
            ], 422);
        }

        $validateAvatarType = Validator::make($credentials, $rules[1]);
        if($validateAvatarType->fails()) {
            return response()->json([
                'error'=> true,
                'message'=> $validateAvatarType->messages()->all(),
                'data' => null
            ], 422);
        }

        $avatar = $request->file('avatar');
            $fileExt = $avatar->getClientOriginalExtension();
            $name =  date("Y-m-d").'_'.time().'.'.$fileExt;
            $avatarName = config('app.url') . "/assets/avatars/$name";

            $storeFile = $avatar->move(public_path('assets/avatars'), $name);

        $update_actor = $actor->update(['avatar_url' => $avatarName]);

        if(!$update_actor) {
            return response()->json([
                'error'=> true,
                'message'=> 'Actor avatar URL could not be updated',
                'data' => null
            ], 400);
        }

        return response()->json([
            'error'=> false,
            'message'=> 'Actor avatar URL updated successfully',
            'data' => null
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     *  @desc Erase all the events
     *  @route DELETE /api/v1/event/erase
     *
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $events = Event::truncate();

        return response()->json([
            'error'=> false,
            'message'=> null,
            'data' => $events
        ], 200);
    }
}
