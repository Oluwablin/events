<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class EventResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id  ?? null,
            "type" => $this->type ?? null,
            "actor" => [
                "id" => $this->actor->id ?? null,
                "login" => $this->actor->login ?? null,
                "avatar_url" => $this->actor->avatar_url ?? null,
            ],
            "repo" => [
                "id" => $this->repo->id ?? null,
                "name" => $this->repo->name ?? null,
                "url" => $this->repo->url ?? null,
            ],
            "created_at" => $this->created_at ?? null,
        ];
    }
}
