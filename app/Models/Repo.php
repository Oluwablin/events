<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Repo extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [''];

    //Relationships
    /**
     * Product has many Events.
     *
     * @return mixed
     */
    public function events()
    {
        return $this->hasMany(Event::class);
    }
}
