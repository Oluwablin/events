<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [''];

    /**
     * Product belongs to a user.
     *
     * @return mixed
     */
    public function actor()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * Product belongs to a user.
     *
     * @return mixed
     */
    public function repo()
    {
        return $this->belongsTo(Repo::class, 'repo_id');
    }
}
